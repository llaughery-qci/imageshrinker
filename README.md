# README #

This repository provides a few scripts which can be run in order to simplify the process of managing device images.  Borrows heavily from https://github.com/Drewsif/PiShrink.
### Scripts ###

* createimage.sh - Creates an image from a specified device.
* pishrink.sh - Shrink a raspberry pi image in such a way that allows it to expand on first boot.
* partshrink.sh - Removes unallocated space from an image with a single partition.

### Prepare InstaGuard Images ###

##### OS Image #####
* Connect SD card to Linux device that has the scripts
* Run 'lsblk' to determine the device path for the SD card (something like /dev/sda)
* Run the createimage script, providing the device path and image destination as arguments like so:
    ~~~~
    ./createimage.sh /dev/sda /dest/of/image.img
    ~~~~
  *Note: Make sure the destination has enough space for the image*
* Run the partshrink script, providing the path to the image created in the last step:
    ~~~~
    ./partshrink.sh /dest/of/image.img
    ~~~~
* A GParted window will open.  Use it to change partition sizes.  When finished, press Apply and close application.
* The image file can now be used to create InstaGuard OS SD cards

##### USB Image #####
* Connect InstaGuard USB Drive to Linux device that has the scripts 
* Run 'lsblk' to determine the device path for the USB (something like /dev/sda)
* Run the createimage script, providing the device path and image destination as arguments like so:
    ~~~~
    ./createimage.sh /dev/sda /dest/of/image.img
    ~~~~
  *Note: Make sure the destination has enough space for the image*
* Run the partshrink script, providing the path to the image created in the last step:
    ~~~~
    ./partshrink.sh /dest/of/image.img
    ~~~~
* A GParted window will open.  Use it to change partition sizes.  When finished, press Apply and close application.
* The image file can now be used to create InstaGuard USB drives

### Prepare Gateway Images ###
##### OS Image #####
* Connect RaspberryPi SD Card to the Linux device that has the scripts
* Run 'lsblk' to determine the device path for the SD card (something like /dev/sda)
* Run the createimage script, providing the device path and image destination as arguments like so:
    ~~~~
    ./createimage.sh /dev/sda /dest/of/image.img
    ~~~~
  *Note: Make sure the destination has enough space for the image*
* Run the pishrink script, providing the image path to be resized.
    ~~~~
    ./pishrink.sh /dest/of/image.img
    ~~~~
* A GParted window will open.  Use it to change partition sizes.  When finished, press Apply and close application.
* Refer to https://github.com/Drewsif/PiShrink for additional assistance.

##### USB Image #####
* Connect Gateway USB Drive to Linux device that has the scripts 
* Run 'lsblk' to determine the device path for the USB (something like /dev/sda)
* Run the createimage script, providing the device path and image destination as arguments like so:
    ~~~~
    ./createimage.sh /dev/sda /dest/of/image.img
    ~~~~
  *Note: Make sure the destination has enough space for the image*
* Run the partshrink script, providing the path to the image created in the last step:
    ~~~~
    ./partshrink.sh /dest/of/image.img
    ~~~~
* A GParted window will open.  Use it to change partition sizes.  When finished, press Apply and close application.
* The image file can now be used to create Gateway USB drives
