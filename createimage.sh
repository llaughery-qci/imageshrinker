#!/bin/bash
usage() { 
  echo "Create image from specified device";
  echo "Usage: $0 /dev/sdx /path/to/image.img "; exit -1; 
}

#Args
dev=$1
img=$2

#Check that what we need is installed
A=`which pv 2>&1`
if (( $? != 0 )); then
  echo "ERROR: pv is not installed. Please run: sudo apt-get install pv"
  exit -4
fi

#Usage checks
if (( EUID != 0 )); then
  echo "ERROR: You need to be running as root."
  exit -1
fi

if [ -z "$dev" ]; then
  echo "ERROR: Please specify a device."
  exit -2
fi

if [ -z "$img" ]; then
  echo "ERROR: Please specify an image destination."
  exit -3
fi

dd if=$dev | pv | dd of=$img bs=1M 

echo "Image created"
