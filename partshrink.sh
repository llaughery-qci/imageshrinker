#!/bin/bash
#https://softwarebakery.com/shrinking-images-on-linux
usage() { 
  echo "Shrinks images composed of a single partition";
  echo "Usage: $0 imagefile.img "; exit -1; 
}

should_skip_autoexpand=false

while getopts ":s" opt; do
  case "${opt}" in
    s) should_skip_autoexpand=true ;;
    *) usage ;;
  esac
done
shift $((OPTIND-1))

#Args
img=$1

#Usage checks
if [[ -z $img ]]; then
  usage
fi
if [[ ! -e $img ]]; then
  echo "ERROR: $img is not a file..."
  exit -2
fi
if (( EUID != 0 )); then
  echo "ERROR: You need to be running as root."
  exit -3
fi

#Check that what we need is installed
A=`which gparted 2>&1`
if (( $? != 0 )); then
  echo "ERROR: gparted is not installed."
  exit -4
fi

#Check number of partitions on image

#Mount image as loopback device
looper=`losetup -f`
losetup $looper $img
partprobe $looper

#Open Gparted
gparted $looper
echo 'Closed gparted'
losetup -d $looper

# #Make sure no more than 2 partitions
# matching=`fdisk -l $img | grep "^$img" | wc -l`
# if (($matching > 2)); then
#   echo "ERROR: Only 2 partitions supported. $matching found.";
#   exit -5;
# fi

# #Resize last partition
# if (($matching > 1)); 
#   then
#     partOneSize=`fdisk -l $img | grep "^$img1" | awk '{ printf(%d,$3) }'`
#     partTwoSize=`fdisk -l $img | grep "^$img1" | awk '{ printf(%d,$3) }'`
#     parted /dev/sda resizepart 1 ($partOneSize + partTwoSize) yes || { echo 'Resize of partition failed' ; exit -4; }
#   else
#     parted /dev/sda resizepart 1 7.5G yes || { echo 'Resize of partition failed' ; exit -4; }
# fi


#Shave partition
allocated=`fdisk -l $img | grep "^$img" | awk '{ sum += $3 } END { print sum; }'`
end=$[($allocated+1)*512]
beforesize=`ls -lah $img | cut -d ' ' -f 5`
truncate --size=$end $img
aftersize=`ls -lah $img | cut -d ' ' -f 5`
echo "Shrunk $img from $beforesize to $aftersize"
